# Grit and Growth Mindset

## 1 Paraphrase (summarize) the video in a few lines. Use your own words.

* Angela Lee Duckworth after leaving the demanding job in management consulting joins teaching math to seventh graders in a New York public school. 

* She realized that IQ was not the only thing that separated successful students from those struggling students.

* She researched in a different field, why and who is successful here.

* She found the way that is grit, tells about grit is passion and perseverance for very long-term goals.

* Grit is having stamina. 

* Grit is continuous practice and working hard to achieve goals.

* Grit is living life like it is a marathon, not a sprint.

* She gives an idea that was developed at Stanford University by Dr.Carol Dweck. Tell them that if you believe that ability to learn new things isn't fixed that will be changed by your effort.

* She said that if you don't believe that failure is a permanent condition.

* Growth mindset is a great idea for building grit.

* We have always been willing to start over again if we fail or are wrong, take a lesson from that and start again. And always need to be gritty about getting our goals.

## 2 What are your key takeaways from the video to take action on?

* Continuous practice gives you a better way to achieve your goals.

* Always be gritty for your goals achieving.

* When we fail to achieve our goals do not stop there, take a lesson learned from failure and start over again.

* Believe that failure is not a permanent condition. Start over it again and continue to achieve our goal.


## 3 Paraphrase (summarize) the video in a few lines in your own words.

* Dr.Dweck tells about mindsets are really important when it comes to learning new things.

* Two ways are mindset first is fixed and second is growth mindsets.

* Differences between both mindsets are belief and focus.

* **Fixed Mindsets:**
    
    * They think that skills are born.
    
    * You can't learn and grow.
    
    * Performance outcomes not looking good.
    
    * Effort is negative.
    
    * Feedback given by others is hated and avoided because they feel that it will not help them.
    
    * Never focused on opportunity.
    
    * Never learned a lesson from their mistakes.


* **Growth Mindsets:**
    
    * They believe that you are in control of your abilities.
    
    * They focus on learning new things and growing.
    
    * They think that progress getting better growth.
    
    * They think that skills are built.
    
    * Effort is useful.
    
    * Feedback given by others is an opportunity to improve ourselves to develop new skills.
    
    * They learned from their mistakes.

* Growth mindsets really create a solid foundation for great learning.

* It,s a fact that you can do better at any age if your mind is growth set. You will improve yourself to good in any skill or field.

## 4 What are your key takeaways from the video to take action on?

* Always have a growth mindset.

* Learned from our mistakes.

* Always take feedback from others for your work and improve where it needs.

* Always ready for an opportunity to improve your skills.

* Always improve yourself to learn new things.

## 5 What is the Internal Locus of Control? What is the key point in the video?

* Internal locus of control is an increase in levels of motivation.

* It gives way to how you feel motivated and believe in yourself to control your life to improve yourself by understanding the problem.

* Always increase motivation to learn new skills or challenge yourself to do it.

* Always having interest or enjoyment while doing any work.

* Always believe in yourself, have control in your life to do hard work, and be motivated during any task.

## 6 Paraphrase (summarize) the video in a few lines in your own words.

* Believe in your ability to figure out things. It will improve me better.

* Question your assumptions. To check whether your assumption is correct or not, you have to change it.

* Develop your own curriculum for growth. Think always about what is your dream and how you will achieve this goal.

* Honor the struggle. Understanding new skill takes some time or may be getting difficulty but never quit their struggle and learn them.

## 7 What are your key takeaways from the video to take action on?

* Always believe in your ability and figure out things that happened and resolve it.

* Ask a question yourself when you assume something it was correct or not. 

* Always having healthy mindsets towards achieving a goal.

* While struggling to learn something give that struggle pride you achieve after that.

## 8 What are one or more points that you want to take action on from the manual? (Maximum 3)

* I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.

* I will stay with a problem till I complete it. I will not quit the problem.

* I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.
