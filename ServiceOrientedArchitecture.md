# Service Oriented Architecture 
 
 * SOA uses a client/server design approach where applications consist of software service consumers. 
 
 * SOA is an architecture for building business applications as a set of loosely coupled black-box components organized to provide well-defined levels of service by interlinking business processes. 
 
 * SOA is a medium for building dynamic, highly configurable collaborative applications designed to accommodate change. 
 
 *  SOA is best used to reduce IT complexity and rigidity. 
 
 * SOA is the best solution to stop  gradual entropy. 
 
 * SOA reduces lead times and costs. 
 
 
 Within a service-oriented architecture, there are two main roles. 
 
 1. **Service Providers:** 
 Service Providers create web services and provide service registrations. Service providers are responsible for the terms of use of their services. 
 
 2. **Service Consumers:** 
 Service consumers can find service metadata in the registry and develop the  client components needed to bind and use the service. 
 
 
 
 ![SOA.md](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-245.png) 
 
 
 
 ## Core Principles of Service Oriented Architecture 
 
 ![SOA.md](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-248.png)
 
 Shown below 9 SOA Design Principles: 
 
 1.**Standardized Service Contracts:** Services follow service descriptions. A service requires information that defines what the service does. 
 
 2.**Loosely-coupled:** Services keep dependencies to a minimum and maintain as many relationships as they need to know about each other. 
 
 3.**Abstraction:** Beyond what is written in the service contract, the service hides logic from the outside world and defines explicit boundaries. 
 
 4.**Reusability:** Logic is divided into services to encourage reuse. 
 
 5.**Autonomy:** Services  control  the logic they encapsulate.
 
 6.**Configurability:** A collection of services can be coordinated and configured, and  are inherently integrated without the need for additional layers of middleware. 
 
 7.**Stateless:** Services keep minimal activity-specific information. 
 
 8.**Discoverability:** Services are designed to be  descriptive in appearance so that they can be discovered and evaluated through available discovery mechanisms. 
 
 9.**Interoperability:** Services must adhere to standards that allow multiple users to access the service. This is  so openly scrutinized today that it is often removed entirely. 
 
 
 ## SOA Characteristics  
 
 Service Oriented Architecture has the following characteristics. 
 
 * Support loose coupling across projects. 
 
 * Location transparent. 
 
 * Self-contained. 
 
 * Improve quality of service. 
 
 * Facilitate discovery and federation. 
 
 * Supports vendor diversity
 
 * Support interoperability. 
 
 * Still a mature and feasible idea. 
 
 
 ## Benefits of SOA 
 
 The benefits are: 
 
 * **Platform Independence:** Services are platform independent because users can interact with separate applications in a common language. 
 
 * **Reusability:** These applications are built from existing services. Therefore, services can be reused to create many applications. 
 
 * **Reliability:** These applications are especially secure because shortcodes are easier to test  than large ones. 
 
 * **Scalability:** Services can run on different servers in the environment. This improves scalability. 
 
 * **Availability:** These facilities are  available immediately to anyone upon request. 
 
 * **Easy Maintenance:** Services are independent and can be easily upgraded and converted without impacting other services. 
 
 * **Parallel Development:** This architecture supports layer-based design. Enables parallel development. 
 
 
 ## SOA Disadvantages  
 
 Disadvantages are: 
 
 * **High Cost:** Costly in terms of human resources, development and technology. 
 
 * **High Bandwidth Servers:** Some web services send and receive messages and information so frequently that they can easily reach 1 million requests per day. Therefore, a fast server with a large amount of data bandwidth is required to run  web services. 
 
 * **High overhead:** Input parameter validation  is performed when services interact. This will increase usage and response time, thus reducing performance.
 
## SOA Framework  
 
 SOA Framework has five horizontal layers. They are defined as: 
 
 1. **Consumer Interface Layer:** A graphical user interface (GUI) app that provides end-user access to your application. 
 
 2. **Business Process Level:** The business use cases associated with the application. 
 
 3. **Service Levels:** This is a complete industry standard service table. 
 
 4. **Service Component Layer:** Managed for developing services. 
 
 5. **OS Layer:** Contains data patterns. 
 
 
 ## Types of SOA Services 
 
 SOA-based services can be broadly classified into three categories. 
 
 1.**Basic Service:** 
 
 ![SOA.md](https://image.slidesharecdn.com/soa-131002021049-phpapp02/85/soa-service-oriented-architecture-45-320.jpg?cb=1668023677)
 
 * Stateless service-oriented software features. 
 
 * Acts as a service provider only. 
 
 * Includes all data access to one data source. 
 
 2.**Intermediary Service:** 
 
 ![](https://image.slidesharecdn.com/soa-131002021049-phpapp02/85/soa-service-oriented-architecture-47-320.jpg?cb=1668023677)
 
  * Stateless services that are both providers and consumers. 

  * Types of businesses and infrastructure are included:

    * **Technology gateways:**
       Span the divide between two technologies.
      
    * **Transformation:**
      Convert the message format to meet the expectations of service consumers and providers.
      
    * **Facades:**
      View of multiple services that have been aggregated and simplified.
      
    * **Functionality-adding services:**
      Add functionality to a service without changing the service itself.
     
3.**Business process services**
  
  ![SOA.md](https://image.slidesharecdn.com/soa-131002021049-phpapp02/85/soa-service-oriented-architecture-49-320.jpg?cb=1668023677)
  
 * Describe your company's business processes . 
 
 * Acts as both  provider and  consumer of services. 
 
 * Tend to be application specific. 
 
 
## References 
 
* https://en.wikipedia.org/wiki/Service-oriented_architecture
 
* https://www.geeksforgeeks.org/service-oriented-architecture
 
* https://www.youtube.com/watch?v=tooRfyCHOT0&t=144s
 
* https://www.javatpoint.com/service-oriented-architecture
 
 
