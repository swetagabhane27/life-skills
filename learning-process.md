# Learning Process

## 1. **What is the Feynman Technique? Paraphrase the video in your own words.**

* Feynman was a great scientist, and a great teacher and was called by nickname "The Great Explainer".

* Feynman Technique is a way to learn any concept faster and to have a deep understanding of that concept. 

* The technique is after learning a topic, you try to teach the concept to a another person. The language should be simple so that the person can grasp everything easily. If you get stuck while teaching, It means that you haven't understood the concept well. You still have some doubts to clear. After clearing the doubt repeat the process until you successfully convey everything about the topic.

## 2. **What are the different ways to implement this technique in your learning process?**

Different ways to implement:-

* Write the name of technique you are learning at top of the paper.

* Explain the concept and do it in a simple language.

* Identify area you are weak in and go back to your resources and material to get a solid grip on it.

* Pinpoint any complicated terms and challenge yourself tp simply them in such a way that you can teach it to even a kid.

## 3. **Paraphrase the video in detail in your own words.**

* Barbara Oakley came from a poor family, she lived in Ten different places. But now she is a Professor Engineer.

* One day his students found out about her past and they ask to, how did u do this? How did you change your brain?
For this Barbara Oakley told me that I am from a poor family and I didn't have money to go to college. so enlisted in the army right out of high school I looked very nervous and I ended up working out on a soviet traveler as a Russian Translator. But she still loved adventure so she ended the job.

* she explained about that how minds work in two ways that are, Focused mode and Diffused mode.

* We understand that while doing some work in deeply feel stuck and we can't able to get a stick on work that is diffused mode. while in diffused mode, we take a break and think freely and go back to focused mode to continue to work.

* Procrastination: First way is you can just kind of keep working a way through it, that within a few minutes it actually will disappear. The second way is just to turn attention away and guess what you feel better.

* Using the Pomodoro technique as it turns out all you need to do is get a timmer that works.

* Another key to keeping exercise within a matter of few days can increase our ability to both learn and to remember.

* Test ourselves all the time.

* Another key to understanding alone is enough to build mastery of the material.

## 4. **What are some of the steps that you can take to improve your learning process?**

* Applying the Pomodoro technique to my workflow.

* If I am stuck on a problem instead of doing that problem, I will take a small break and then start with a fresh mind setup. 

* Taking proper sleep and eating healthy food. 

* I will practice some exercises as they can improve my learning ability.


## 5. **Your key takeaways from the video? Paraphrase your understanding.**


* You have to enjoy whenever start a  new skills.

* Researching websites and books says it takes 10000 hours to acquire or learn a new skill.

* K. Ericcson Anderson is the founder of this 10000-hour rule.

* He studied it from famous athletes, Chess grand Masters, etc.

* But according to Josh Kaufman, you truly need at least 20 hours of fully focused dedication and practice to learn new skills from the very basics to advance.

* Be ultra clear about what you want to achieve after learning that skill. Break down the skills into smaller and smaller pieces.

* Do not get distracted while doing the work. Remove all the distractions from your eyesight.

* Going through the first couple of hrs is very frustrating as we don't know anything about that topic. But one should be determined enough to complete it.

## 6. **What are some of the steps that you can while approaching a new topic?**

* Deconstruct the skill: look into the skill and break it into smaller things and practice.

* Learn enough to self-correct: just self-learn is enough actual practice and correcting by ourselves.

* Remove practice barriers: remove all distractions that are keeping you from practicing.

* Practice for 20 hours: whenever starting new learning skill brings initial frustration, you will be able to overcome that frustration barrier and practice well to learn a skill.







